/*******************************************************************************

    ηMatrix - a browser extension to black/white list requests.
    Copyright (C) 2014-2019 Raymond Hill
    Copyright (C) 2019-2025 Alessio Vanni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.

    Home: https://gitlab.com/vannilla/ematrix
    uMatrix Home: https://github.com/gorhill/uMatrix
*/

'use strict';

// ORDER IS IMPORTANT

// Load everything

(function() {
    let ηm = ηMatrix;

    let processCallbackQueue = function (queue, callback) {
	while (queue.length > 0) {
	    let fn = queue.pop();
	    fn();
	}

	if (typeof callback === 'function') {
	    callback();
	}
    };

    let onAllDone = function () {
	ηm.webRequest.start();

	ηm.assets.checkVersion();
	ηm.assets.addObserver(ηm.assetObserver.bind(ηm));
	ηm.scheduleAssetUpdater(ηm.userSettings.autoUpdate ? 7 * 60 * 1000 : 0);

	vAPI.cloud.start([ 'myRulesPane' ]);
    };

    let onTabsReady = function (tabs) {
	for (let i=tabs.length-1; i>=0; --i) {
	    // console.debug('start.js > binding %d tabs', i);
            let tab = tabs[i];
            ηm.tabContextManager.push(tab.id, tab.url, 'newURL');
	}

	onAllDone();
    };

    let onUserSettingsLoaded = function () {
	ηm.loadHostsFiles();
    };

    let onPSLReady = function () {
	ηm.loadUserSettings(onUserSettingsLoaded);
	ηm.loadRawSettings();
	ηm.loadMatrix();

	// rhill 2013-11-24: bind behind-the-scene virtual tab/url
	// manually, since the normal way forbid binding behind the
	// scene tab.
	// https://github.com/gorhill/httpswitchboard/issues/67
	ηm.pageStores[vAPI.noTabId] =
	    ηm.pageStoreFactory(ηm.tabContextManager.mustLookup(vAPI.noTabId));
	ηm.pageStores[vAPI.noTabId].title =
	    vAPI.i18n('statsPageDetailedBehindTheScenePage');

	vAPI.tabs.getAll(onTabsReady);
    };

    processCallbackQueue(ηm.onBeforeStartQueue, function () {
	ηm.loadPublicSuffixList(onPSLReady);
    });
})();
