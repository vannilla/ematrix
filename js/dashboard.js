/*******************************************************************************

    ηMatrix - a browser extension to black/white list requests.
    Copyright (C) 2014-2019 Raymond Hill
    Copyright (C) 2019-2025 Alessio Vanni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.

    Home: https://gitlab.com/vannilla/ematrix
    uMatrix Home: https://github.com/gorhill/uMatrix
*/

'use strict';

(function() {
    let loadDashboardPanel = function (hash) {
        var button = uDom(hash);
        var url = button.attr('data-dashboard-panel-url');
        uDom('iframe').attr('src', url);
        uDom('.tabButton').forEach(function (button) {
            button.toggleClass('selected',
			       button.attr('data-dashboard-panel-url') === url);
        });
    };

    let onTabClickHandler = function () {
        loadDashboardPanel(window.location.hash);
    };

    uDom.onLoad(function () {
        window.addEventListener('hashchange', onTabClickHandler);
        let hash = window.location.hash;
        if (hash.length < 2) {
            hash = '#settings';
        }
        loadDashboardPanel(hash);
    });
})();
