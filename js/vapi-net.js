/*******************************************************************************

    ηMatrix - a browser extension to black/white list requests.
    Copyright (C) 2014-2019 The uMatrix/uBlock Origin authors
    Copyright (C) 2019-2025 Alessio Vanni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.

    Home: https://gitlab.com/vannilla/ematrix
    uMatrix Home: https://github.com/gorhill/uMatrix
*/

'use strict';

/******************************************************************************/

(function () {
    Cu.import('chrome://ematrix/content/lib/PendingRequests.jsm');
    
    vAPI.net = {};

    vAPI.net.registerListeners = function () {
	this.onBeforeRequest.types = this.onBeforeRequest.types
	    ? new Set(this.onBeforeRequest.types)
	    : null;
	
	this.onBeforeSendHeaders.types = this.onBeforeSendHeaders.types
	    ? new Set(this.onBeforeSendHeaders.types)
	    : null;

	let shouldLoadListenerMessageName = location.host + ':shouldLoad';
	let shouldLoadListener = function (e) {
            let details = e.data;
            let pendingReq =
		PendingRequestBuffer.createRequest(details.url);
            pendingReq.rawType = details.rawType;
            pendingReq.tabId = vAPI.tabs.manager.tabIdFromTarget(e.target);
	};

	// https://github.com/gorhill/uMatrix/issues/200
	// We need this only for Firefox 34 and less: the tab id is derived from
	// the origin of the message.
	if (!vAPI.modernFirefox) {
            vAPI.messaging.globalMessageManager
		.addMessageListener(shouldLoadListenerMessageName,
				    shouldLoadListener);
	}

	vAPI.httpObserver.register();

	vAPI.addCleanUpTask(function () {
            if (!vAPI.modernFirefox) {
		vAPI.messaging.globalMessageManager
		    .removeMessageListener(shouldLoadListenerMessageName,
					   shouldLoadListener);
            }

            vAPI.httpObserver.unregister();
	});
    };
})();
