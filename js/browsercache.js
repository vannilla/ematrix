/*******************************************************************************

    ηMatrix - a browser extension to black/white list requests.
    Copyright (C) 2015-2019 Raymond Hill
    Copyright (C) 2019-2025 Alessio Vanni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.

    Home: https://gitlab.com/vannilla/ematrix
    uMatrix Home: https://github.com/gorhill/uMatrix
*/

'use strict';

(function () {
    // Browser data jobs

    let clearCache = function () {
	vAPI.setTimeout(clearCache, 15 * 60 * 1000);

	var ηm = ηMatrix;
	if (!ηm.userSettings.clearBrowserCache) {
            return;
	}

	ηm.clearBrowserCacheCycle -= 15;
	if (ηm.clearBrowserCacheCycle > 0) {
            return;
	}

	vAPI.browser.data.clearCache();

	ηm.clearBrowserCacheCycle = ηm.userSettings.clearBrowserCacheAfter;
	++ηm.browserCacheClearedCounter;

	// TODO: i18n
	ηm.logger.writeOne('', 'info',
			   vAPI.i18n('loggerEntryBrowserCacheCleared'));

	// console.debug('clearBrowserCacheCallback()> '
	// 	      + 'vAPI.browser.data.clearCache() called');
    };

    vAPI.setTimeout(clearCache, 15 * 60 * 1000);
})();
