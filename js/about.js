/*******************************************************************************

    ηMatrix - a browser extension to black/white list requests.
    Copyright (C) 2014-2019 Raymond Hill
    Copyright (C) 2019-2025 Alessio Vanni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.

    Home: https://gitlab.com/vannilla/ematrix
    uMatrix Home: https://github.com/gorhill/uMatrix
*/

/* global uDom */

'use strict';

uDom.onLoad(function () {

    let backupUserDataToFile = function () {
	let userDataReady = function (userData) {
            vAPI.download({
		'url': 'data:text/plain,'
		    + encodeURIComponent(JSON.stringify(userData, null, 2)),
		'filename': uDom('[data-i18n="aboutBackupFilename"]').text()
            });
	};

	vAPI.messaging.send('about.js', {
	    what: 'getAllUserData'
	}, userDataReady);
    };

    let restoreUserDataFromFile = function () {
	let validateBackup = function (s) {
            let userData = null;

            try {
		userData = JSON.parse(s);
            }
            catch (e) {
		userData = null;
            }

            if (userData === null
		|| typeof userData !== 'object'
		|| typeof userData.version !== 'string'
		|| typeof userData.when !== 'number'
		|| typeof userData.settings !== 'object'
		|| typeof userData.rules !== 'string'
		|| typeof userData.hostsFiles !== 'object') {
		return null;
            }

            return userData;
	};

	let fileReaderOnLoadHandler = function () {
            let userData = validateBackup(this.result);
            if (!userData) {
		window.alert(uDom('[data-i18n="aboutRestoreError"]').text());
		return;
            }
            let time = new Date(userData.when);
            let msg =
		uDom('[data-i18n="aboutRestoreConfirm"]')
		.text()
		.replace('{{time}}', time.toLocaleString());
            let proceed = window.confirm(msg);
            if (proceed) {
		vAPI.messaging.send('about.js', {
		    what: 'restoreAllUserData',
		    userData: userData
		});
            }
	};

	let file = this.files[0];
	if (file === undefined || file.name === '') {
            return;
	}
	if (file.type.indexOf('text') !== 0) {
            return;
	}
	let fr = new FileReader();
	fr.onload = fileReaderOnLoadHandler;
	fr.readAsText(file);
    }

    let startRestoreFilePicker = function () {
	let input = document.getElementById('restoreFilePicker');
	// Reset to empty string, this will ensure a change event is
	// properly triggered if the user pick a file, even if it is
	// the same as the last one picked.
	input.value = '';
	input.click();
    };

    let resetUserData = function () {
	let proceed =
	    window.confirm(uDom('[data-i18n="aboutResetConfirm"]').text());
	if (proceed) {
            vAPI.messaging.send('about.js', {
		what: 'resetAllUserData'
	    });
	}
    };

    (function () {
	let renderStats = function (details) {
            document.getElementById('aboutVersion')
		.textContent = details.version;
            let template = uDom('[data-i18n="aboutStorageUsed"]').text();
            let storageUsed = '?';
            if (typeof details.storageUsed === 'number') {
		storageUsed = details.storageUsed.toLocaleString();
            }
            document.getElementById('aboutStorageUsed').textContent =
		template.replace('{{storageUsed}}', storageUsed);
	};
	vAPI.messaging.send('about.js', {
	    what: 'getSomeStats'
	}, renderStats);
    })();

    uDom('#backupUserDataButton').on('click', backupUserDataToFile);
    uDom('#restoreUserDataButton').on('click', startRestoreFilePicker);
    uDom('#restoreFilePicker').on('change', restoreUserDataFromFile);
    uDom('#resetUserDataButton').on('click', resetUserData);
});
