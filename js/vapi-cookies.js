/*******************************************************************************

    ηMatrix - a browser extension to black/white list requests.
    Copyright (C) 2014-2019 The uMatrix/uBlock Origin authors
    Copyright (C) 2019-2025 Alessio Vanni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.

    Home: https://gitlab.com/vannilla/ematrix
    uMatrix Home: https://github.com/gorhill/uMatrix
*/

'use strict';

/******************************************************************************/

(function () {
    vAPI.cookies = {};

    vAPI.cookies.CookieEntry = function (ffCookie) {
	this.domain = ffCookie.host;
	this.name = ffCookie.name;
	this.path = ffCookie.path;
	this.secure = ffCookie.isSecure === true;
	this.session = ffCookie.expires === 0;
	this.value = ffCookie.value;
    };

    vAPI.cookies.start = function () {
	Services.obs.addObserver(this, 'cookie-changed', false);
	Services.obs.addObserver(this, 'private-cookie-changed', false);
	vAPI.addCleanUpTask(this.stop.bind(this));
    };

    vAPI.cookies.stop = function () {
	Services.obs.removeObserver(this, 'cookie-changed');
	Services.obs.removeObserver(this, 'private-cookie-changed');
    };

    vAPI.cookies.observe = function (subject, topic, reason) {
	//if ( topic !== 'cookie-changed' && topic !== 'private-cookie-changed' ) {
	//    return;
	//}
	//
	if (reason === 'cleared' && typeof this.onAllRemoved === 'function') {
            this.onAllRemoved();
            return;
	}
	if (subject === null) {
            return;
	}
	if (subject instanceof Ci.nsICookie2 === false) {
            try {
		subject = subject.QueryInterface(Ci.nsICookie2);
            } catch (ex) {
		return;
            }
	}
	if (reason === 'deleted') {
            if (typeof this.onRemoved === 'function') {
		this.onRemoved(new this.CookieEntry(subject));
            }
            return;
	}
	if (typeof this.onChanged === 'function') {
            this.onChanged(new this.CookieEntry(subject));
	}
    };

    vAPI.cookies.getAll = function(callback) {
	// Meant and expected to be asynchronous.
	if (typeof callback !== 'function') {
            return;
	}
	
	let onAsync = function () {
            let out = [];
            let enumerator = Services.cookies.enumerator;
            let ffcookie;
            while (enumerator.hasMoreElements()) {
		ffcookie = enumerator.getNext();
		if (ffcookie instanceof Ci.nsICookie) {
                    out.push(new this.CookieEntry(ffcookie));
		}
            }
	    
            callback(out);
	};
	
	vAPI.setTimeout(onAsync.bind(this), 0);
    };

    vAPI.cookies.remove = function (details, callback) {
	let uri = Services.io.newURI(details.url, null, null);
	let cookies = Services.cookies;
	cookies.remove(uri.asciiHost, details.name, uri.path, false, {});
	cookies.remove( '.' + uri.asciiHost, details.name, uri.path, false, {});
	
	if (typeof callback === 'function') {
            callback({
		domain: uri.asciiHost,
		name: details.name,
		path: uri.path
            });
	}
    };
})();
