# ηMatrix

Definitely for advanced users.

## Quickstart

The extension is fairly hard to use for beginners, so you might want
to do the following:

+ Before you install it, read the full description as shown in the
  add-on repository (linked later);
+ Read the section titled *Warning* at the end of this file;
+ After installing the extension, click on the toolbar button then on
  the black bar at the top of the popup (or use the button labeled
  _Dashboard_ in the about:addons preferences), navigate to the
  _About_ tab and click the _Documentation_ link;
+ The linked site contains tips and tricks on how to use the
  extension, so if you are stuck with something consult those
  articles.

## Install

The extension can be installed either by obtaining it from the
official add-ons websites, or by manually building the XPI.

### Official add-on repository

+ Pale Moon: http://addons.palemoon.org/addon/ematrix/
+ Basilisk: https://addons.basilisk-browser.org/addon/ematrix/

### Building the XPI

Clone the repository with git, then run `make all`.  
The XPI will be called `eMatrix.xpi`.

## Warning

### Regarding broken sites

ηMatrix will definitely break the majority of websites you visit.  
This add-on is meant for people that understand this and have enough
knowledge to understand what it needs to be done to "unbreak" those
site.

### About the name

Many people mistake the first letter of the name for a lowercase latin
letter N, but it is actually the lowercase greek letter eta.

## License

[GPLv3](LICENSE.txt)
