/*******************************************************************************

    ηMatrix - a browser extension to black/white list requests.
    Copyright (C) 2014-2019 The µBlock authors
    Copyright (C) 2019-2025 Alessio Vanni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.

    Home: https://gitlab.com/vannilla/ematrix
    uMatrix Home: https://github.com/gorhill/uMatrix
*/

'use strict';

Components.utils.import('chrome://ematrix/content/lib/FrameModule.jsm');

var locationChangeListener; // Keep alive while frameScript is alive

(function () {
    let injectContentScripts = function (win) {
	if (!win || !win.document) {
            return;
	}

	contentObserver.observe(win.document);

	if (win.frames && win.frames.length) {
	    for (let i = win.frames.length; i>0; --i) {
		injectContentScripts(win.frames[i]);
            }
	}
    };

    let onLoadCompleted = function () {
	removeMessageListener('ematrix-load-completed', onLoadCompleted);
	injectContentScripts(content);
    };

    addMessageListener('ematrix-load-completed', onLoadCompleted);

    if (docShell) {
	let Ci = Components.interfaces;
	let wp = docShell
	    .QueryInterface(Ci.nsIInterfaceRequestor)
	    .getInterface(Ci.nsIWebProgress);
	let dw = wp.DOMWindow;
	
	if (dw === dw.top) {
            locationChangeListener = new LocationChangeListener(docShell);
	}
    }
})();
