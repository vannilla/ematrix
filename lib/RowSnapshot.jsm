/*******************************************************************************

    ηMatrix - a browser extension to black/white list requests.
    Copyright (C) 2014-2019 Raymond Hill
    Copyright (C) 2019-2020-2021 Alessio Vanni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.

    Home: https://gitlab.com/vannilla/ematrix
    uMatrix Home: https://github.com/gorhill/uMatrix
*/

'use strict';

// Apparently, for this specific data structure using a constructor is
// faster than an object literal.
// I don't really believe it, but let's keep things as they are.
// Now a module to keep things more organized (might make things slower.)

var EXPORTED_SYMBOLS = ['RowSnapshot'];

var RowSnapshot = function (matrix, src, dest, domain) {
    this.domain = domain;
    this.temporary = matrix.tMatrix.evaluateRowZXY(src, dest);
    this.permanent = matrix.pMatrix.evaluateRowZXY(src, dest);
    this.counts = RowSnapshot.counts(matrix).slice();
    this.totals = RowSnapshot.counts(matrix).slice();
};

RowSnapshot.memoizedCounts = undefined;

RowSnapshot.counts = function (matrix) {
    if (RowSnapshot.memoizedCounts !== undefined) {
	return RowSnapshot.memoizedCounts;
    }

    let aa = [];
    let n = matrix.Matrix.columnHeaderIndices.size;

    for (let i=0; i<n; ++i) {
        aa[i] = 0;
    }

    RowSnapshot.memoizedCounts = aa;

    return aa;
};
