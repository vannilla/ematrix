/*******************************************************************************

    ηMatrix - a browser extension to black/white list requests.
    Copyright (C) 2014-2019 The uMatrix/uBlock Origin authors
    Copyright (C) 2019-2020-2021 Alessio Vanni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.

    Home: https://gitlab.com/vannilla/ematrix
    uMatrix Home: https://github.com/gorhill/uMatrix
*/

'use strict';

var EXPORTED_SYMBOLS = ['PendingRequestBuffer'];

function PendingRequest() {
    this.rawType = 0;
    this.tabId = 0;
    this._key = '';
}

var bufferLength = 1024
var urlToIndex = new Map();
var writePointer = 0;
var ringBuffer = new Array(bufferLength);

for (let i=0; i<bufferLength; ++i) {
    ringBuffer[i] = new PendingRequest();
}

var PendingRequestBuffer = {
    createRequest: function (url) {
	// URL to ring buffer index map:
	// { k = URL, s = ring buffer indices }
	//
	// s is a string which character codes map to ring buffer
	// indices -- for when the same URL is received multiple times
	// by shouldLoadListener() before the existing one is serviced
	// by the network request observer.  I believe the use of a
	// string in lieu of an array reduces memory churning.
	let bucket;
	let i = writePointer;
	writePointer = (i + 1) % bufferLength;

	let req = ringBuffer[i];
	let str = String.fromCharCode(i);

	if (req._key !== '') {
	    bucket = urlToIndex.get(req._key);
	    if (bucket.lenght === 1) {
		urlToIndex.delete(req._key);
	    } else {
		let pos = bucket.indexOf(str);
		urlToIndex.set(req._key,
			       bucket.slice(0, pos)+bucket.slice(pos+1));
	    }
	}

	bucket = urlToIndex.get(url);
	urlToIndex.set(url,
		       (bucket === undefined) ? str : bucket + str);
	req._key = url;

	return req;
    },
    lookupRequest: function (url) {
	let bucket = urlToIndex.get(url);
	if (bucket === undefined) {
	    return null;
	}

	let i = bucket.charCodeAt(0);
	if (bucket.length === 1) {
	    urlToIndex.delete(url);
	} else {
	    urlToIndex.set(url, bucket.slice(1));
	}

	let req = ringBuffer[i];
	req._key = '';
	
	return req;
    },
};
