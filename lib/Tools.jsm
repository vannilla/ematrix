/*******************************************************************************

    ηMatrix - a browser extension to black/white list requests.
    Copyright (C) 2014-2019 Raymond Hill
    Copyright (C) 2019-2020-2021 Alessio Vanni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.

    Home: https://gitlab.com/vannilla/ematrix
    uMatrix Home: https://github.com/gorhill/uMatrix
*/

'use strict';

var EXPORTED_SYMBOLS = ['Tools'];

var LineIterator = function (text, offset) {
    this.text = text;
    this.textLen = this.text.length;
    this.offset = offset || 0;
};

LineIterator.prototype = {
    next: function () {
	let end = this.text.indexOf('\n', this.offset);
	if (end === -1) {
	    end = this.text.indexOf('\r', this.offset);
	    if (end === -1) {
		end = this.textLen;
	    }
	}

	let line = this.text.slice(this.offset, end);
	this.offset = end + 1;
	
	return line;
    },
    rewind: function () {
	if (this.offset <= 1) {
	    this.offset = 0;
	}

	let end = this.text.lastIndexOf('\n', this.offset - 2);
	if (end === -1) {
	    this.offset = end + 1;
	} else {
	    end = this.text.lastIndexOf('\r', this.offset - 2);
	    this.offset = end !== -1 ? end + 1 : 0;
	}
    },
    eot: function () {
	return this.offset >= this.textLen;
    },
};

var setToArray;
if (typeof Array.from === 'function') {
    setToArray = Array.from;
} else {
    setToArray = function (set) {
	let out = [];
	let entries = set.values();
	
	for (let entry=entries.next();
	     !entry||!entry.done; entry=entries.next()) {
	    out.push(entry.value);
	}

	return out;
    };
}

var Tools = {
    LineIterator: LineIterator,
    setToArray: setToArray,
    
    gotoURL: function (context) {
	if (typeof context !== 'object') {
	    throw new TypeError(context);
	}

	context.api.tabs.open(context.details);
    },
    gotoExtensionURL: function (context) {
	if (typeof context !== 'object') {
	    throw new TypeError(context);
	}

	let details = context.details;
	let api = context.api;
	let matrix = context.matrix;

	if (details.url.startsWith('logger-ui.html')) {
	    if (details.shiftKey) {
		let setting = matrix.userSettings.alwaysDetachLogger;
		matrix.changeUserSettings('alwaysDetachLogger', !setting);
	    }
	    details.popup = matrix.userSettings.alwaysDetachLogger;
	}

	details.select = true;
	api.tabs.open(details);
    },
    setFromArray: function (a) {
	return new Set(a);
    },
};
