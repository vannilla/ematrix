/*******************************************************************************

    ηMatrix - a browser extension to black/white list requests.
    Copyright (C) 2014-2019 The uMatrix/uBlock Origin authors
    Copyright (C) 2020 Alessio Vanni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.

    Home: https://gitlab.com/vannilla/ematrix
    uMatrix Home: https://github.com/gorhill/uMatrix
*/

'use strict';

var Ci = Components.interfaces;

var EXPORTED_SYMBOLS = ['HostMap'];

var map = new Map();

var HostMap = {
    put: function (key, value) {
	if (typeof key !== 'string' || !(value instanceof Ci.nsIURI)
	    || !key || !value) {
	    throw new Error('invalid argument(s)');
	}

	if (map.get(key)) {
	    return;
	}

	map.set(key, value);
    },
    get: function (key) {
	if (typeof key !== 'string' || !key) {
	    throw new Error('invalid argument');
	}

	return map.get(key);
    },
};
